﻿//Daniel Castaño Estrella
//daniel.c.estrella@gmail.com
//2019

using UnityEngine;
using UnityEngine.SpatialTracking;

namespace DanielEstrella.GrabbingDemo
{
	[RequireComponent(typeof(PoseProvider))]
	public class Hand : MonoBehaviour
	{
		[SerializeField] private GameObject grabbable;

		private PoseProvider poseProvider;
		private bool grabbed = false;

		public PoseProvider PoseProvider { get { if (!poseProvider) poseProvider = GetComponent<PoseProvider>(); return poseProvider; } }

[ContextMenu("Grab")]
		public void Grab()
		{
			grabbable.transform.parent = null;
			grabbable.transform.position = Vector3.zero;
			grabbable.transform.rotation = Quaternion.identity;

			TrackedPoseDriver tpd = grabbable.AddComponent<TrackedPoseDriver>();
			tpd.UseRelativeTransform = true;
			tpd.poseProviderComponent = PoseProvider;
		}

		[ContextMenu("Release")]
		public void Release()
		{
			DestroyImmediate(grabbable.GetComponent<TrackedPoseDriver>());
			grabbable.transform.parent = null;
			grabbable.transform.position = transform.position;
			grabbable.transform.rotation = transform.rotation;
		}

		private void Update()
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				if (!grabbed)
				{
					Grab();
					grabbed = true;
				}
				else
				{
					Release();
					grabbed = false;
				}
			}
		}
	}
}