﻿//Daniel Castaño Estrella
//daniel.c.estrella@gmail.com
//2019

using UnityEngine;
using UnityEngine.Experimental.XR.Interaction;

namespace DanielEstrella.GrabbingDemo
{
	public class PoseProvider : BasePoseProvider
	{
		public override bool TryGetPoseFromProvider(out Pose output)
		{
			output = new Pose(transform.position, transform.rotation);

			return gameObject.activeInHierarchy;
		}
	}
}